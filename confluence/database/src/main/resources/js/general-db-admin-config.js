databaseConfigHelper = (function($) {

    var methods = new Object();
    var url = AJS.contextPath() + "/rest/database/1.0/admin-config/configuration";
    // This dictates whether the stored xml is still valid.
    // Follows the Semver standard (First digit indicates breaking change).
    var schemaVersionString = "1.1.0";
    var schemaVersionArr = schemaVersionString.split(".");

    methods['saveJdbcDriverDirectory'] = function() {
        saveConfig();
        location.reload();
    }

    methods['saveConfigAuth'] = function() {
        saveConfig();
    }

    methods['addGroup'] = function() {
        if (AJS.$("#group-list").length == 0) {
            AJS.$("#group-list-container").html("<ul id=\"group-list\"></ul>");
        }

        appendGroup(AJS.$("#new-group-name").val());
        AJS.$("#new-group-name").val("");

        saveConfig();
    }

    methods['removeEntry'] = function(e) {
        e.preventDefault();
        $(e.currentTarget).parent().remove();

        if (AJS.$(".saved-group").length == 0) {
            AJS.$("#group-list-container").html("<div class=\"none-saved\">No saved groups</div>");
        }

        if (AJS.$(".saved-address").length == 0) {
            AJS.$("#email-list-container").html("<div class=\"none-saved\">No saved addresses.</div>");
        }

        saveConfig();
    }

    methods['loadConfig'] = function() {
        $.ajax({
            url: url,
            dataType: "json"
        }).done(function(pluginConfiguration) {
            // If XML doesn't validate, just give an empty document
            try {
                $xml = AJS.$(AJS.$.parseXML(decodeURIComponent(pluginConfiguration.xml)));
            } catch(err) {
                if( pluginConfiguration.xml != null){
                   console.error("Failed to parse XML: ", err);
                }
                $xml = AJS.$(AJS.$.parseXML(
                    "<plugin-configuration>\n"+
                    "    <jdbc-driver-directory></jdbc-driver-directory>\n"+
                    "    <config-authorized-users></config-authorized-users>\n"+
                    "    <config-authorized-groups></config-authorized-groups>\n"+
                    "    <row-limit-type></row-limit-type>\n"+
                    "    <row-limit-value></row-limit-value>\n"+
                    "    <timeout-limit-type></timeout-limit-type>\n"+
                    "    <timeout-limit-value></timeout-limit-value>\n"+
                    "    <log-email></log-email>\n"+
                    "    <notification-email></notification-email>\n"+
                    "</plugin-configuration>"
                ));
            }

            // Check for breaking changes in the schema
            if ($xml.find("schema-version").length != 0) {
                var xmlVersion = $xml.find("schema-version").html();
                var xmlMajorVer = xmlVersion.split(".")[0];
                if (xmlMajorVer != schemaVersionArr[0]) {
                    $xml = AJS.$(AJS.$.parseXML(
                        "<plugin-configuration>\n"+
                        "    <jdbc-driver-directory></jdbc-driver-directory>\n"+
                        "    <config-authorized-users></config-authorized-users>\n"+
                        "    <config-authorized-groups></config-authorized-groups>\n"+
                        "    <row-limit-type></row-limit-type>\n"+
                        "    <row-limit-value></row-limit-value>\n"+
                        "    <timeout-limit-type></timeout-limit-type>\n"+
                        "    <timeout-limit-value></timeout-limit-value>\n"+
                        "    <log-email></log-email>\n"+
                        "    <notification-email></notification-email>\n"+
                        "</plugin-configuration>"
                    ));
                }
            }

            $("#jdbc-driver-directory").val(atob($xml.find("jdbc-driver-directory").html()));
            var configAuthorizedUsers = atob($xml.find("config-authorized-users").html());
            var configAuthorizedGroups = atob($xml.find("config-authorized-groups").html());
            var rowLimitType = atob($xml.find("row-limit-type").html());
            var rowLimitValue = atob($xml.find("row-limit-value").html());
            var timeoutLimitType = atob($xml.find("timeout-limit-type").html());
            var timeoutLimitValue = atob($xml.find("timeout-limit-value").html());
            var notifyAll = $xml.find("log-email").html();
            var notificationEmail = atob($xml.find("notification-email").html());

            // If we failed to get the fields from the xml, default to empty string;
            if (configAuthorizedUsers == null) {
                configAuthorizedUsers = "";
            }
            if (configAuthorizedGroups == null) {
                configAuthorizedGroups = "";
            }
            if (rowLimitType == null) {
                rowLimitType = "Soft";
            }
            if (rowLimitValue == null) {
                rowLimitValue = "";
            }
            if (timeoutLimitType == null) {
                timeoutLimitType = "Soft";
            }
            if (timeoutLimitValue == null) {
                timeoutLimitValue = "";
            }
            if (notificationEmail == null) {
                notificationEmail = "";
            }

            // Load access profiles for this page.
            AJS.$("#configuration-permissions").append(Keysight.Database.Admin.Config.Templates.accessProfile({
                configAuthorizedUsers: configAuthorizedUsers,
                configAuthorizedGroups: configAuthorizedGroups
            }));

            AJS.$("#row-limit-type").val( rowLimitType );
            AJS.$("#row-limit-value").val( rowLimitValue );
            AJS.$("#timeout-limit-type").val( timeoutLimitType );
            AJS.$("#timeout-limit-value").val( timeoutLimitValue );
            AJS.$("#log-email").val(notifyAll);
            AJS.$("#notification-email").val( notificationEmail );

            AJS.$("#save-config-auth").click(function(e) {
                e.preventDefault();
                databaseConfigHelper.saveConfigAuth();
            });

            AJS.$("#save-performance-security-control-settings").click(function(e) {
                e.preventDefault();
                databaseConfigHelper.saveConfigAuth();
            });

        }).fail(function(self, status, error) {
            var loadFlag = AJS.flag({
                type: 'error',
                title: 'Failed to fetch configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }

    function saveConfig() {
        var xmlString = '<?xml version="1.0" encoding="UTF-8"?>' + "\n" +
            '<plugin-configuration>' + "\n" +
               '<schema-version>'+schemaVersionString+'</schema-version>' + "\n" +
            '   <jdbc-driver-directory>' + btoa($("#jdbc-driver-directory").val()) + "</jdbc-driver-directory>\n" +
            '   <config-authorized-users>' + btoa($("#config-authorized-users").val()) + "</config-authorized-users>\n" +
            '   <config-authorized-groups>' + btoa($("#config-authorized-groups").val()) + "</config-authorized-groups>\n" +
            '   <row-limit-type>' + btoa($("#row-limit-type").val()) + "</row-limit-type>\n" +
            '   <row-limit-value>' + btoa($("#row-limit-value").val()) + "</row-limit-value>\n" +
            '   <timeout-limit-type>' + btoa($("#timeout-limit-type").val()) + "</timeout-limit-type>\n" +
            '   <timeout-limit-value>' + btoa($("#timeout-limit-value").val()) + "</timeout-limit-value>\n" +
            '   <log-email>' + $("#log-email").val() + "</log-email>\n" +
            '   <notification-email>' + btoa($("#notification-email").val()) + "</notification-email>\n" +
            '</plugin-configuration>' + "\n";

        // Validate XML
        try {
            AJS.$(AJS.$.parseXML(xmlString))
        } catch(err) {
            console.error("Malformed XML!: ", err);
            return;
        }

        AJS.$.ajax({
            url: url,
            type: "PUT",
            contentType: "application/json",
            data: '{"xml":"' + encodeURIComponent(xmlString) + '"}',
            processData: false
        }).done(function() {
            var saveSuccessFlag = AJS.flag({
                type: 'success',
                title: 'Success!',
                body: 'Plugin configuration was saved successfully.',
                close: 'auto'
            });
        }).fail(function(self, status, error) {
            var saveFailFlag = AJS.flag({
                type: 'error',
                title: 'Failed to save configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }

    return methods;
})(AJS.$ || jQuery);

AJS.toInit(function() {

    AJS.$("#save-jdbc-driver-directory").click(function(e) {
        e.preventDefault();
        databaseConfigHelper.saveJdbcDriverDirectory();
    });

    databaseConfigHelper.loadConfig();
});