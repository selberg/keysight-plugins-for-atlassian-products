package com.keysight.database.helpers;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
 
/**
 * This class is used to replace <component-import /> declarations in the atlassian-plugin.xml.
 * This class will be scanned by the atlassian spring scanner at compile time.
 * There are no situations this class needs to be instantiated, it's sole purpose 
 * is to collect the component-imports in the one place.
 *
 * The list of components that may be imported can be found at: <your-confluence-url>/admin/pluginexports.action
 */

@SuppressWarnings("UnusedDeclaration")
@Scanned
public class ComponentImports
{
    @ComponentImport com.atlassian.confluence.pages.PageManager                     pageManager;
    @ComponentImport com.atlassian.confluence.plugin.services.VelocityHelperService velocityHelperService;
    @ComponentImport com.atlassian.confluence.setup.settings.SettingsManager        settingsManager;
    @ComponentImport com.atlassian.confluence.spaces.SpaceManager                   spaceManager;
    @ComponentImport com.atlassian.confluence.user.UserAccessor                     userAccessor;
    @ComponentImport com.atlassian.confluence.xhtml.api.XhtmlContent                xhtmlUtils;
    @ComponentImport com.atlassian.core.task.MultiQueueTaskManager                  multiQueueTaskManager;
    @ComponentImport com.atlassian.sal.api.auth.LoginUriProvider                    loginUriProvider;
    @ComponentImport com.atlassian.sal.api.pluginsettings.PluginSettingsFactory     pluginSettingsFactory;
    @ComponentImport com.atlassian.sal.api.transaction.TransactionTemplate          transactionTemplate;
    @ComponentImport com.atlassian.sal.api.user.UserManager                         userManager;
    @ComponentImport com.atlassian.templaterenderer.TemplateRenderer                templateRenderer;

    private ComponentImports()
    {
        throw new Error("This class should not be instantiated");
    }
}
